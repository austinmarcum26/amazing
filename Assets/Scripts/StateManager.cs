﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class StateManager : MonoBehaviour
{
    public void LoadNextLevel() {
        SceneManager.LoadScene("MazeScene");
    }
}
