﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float jumpForce;
    public float speed;
    public float gravity = 30f;
    private Vector3 moveDirection = Vector3.zero;

    // Called once per frame
    void FixedUpdate() {
        CharacterController controller = gameObject.GetComponent<CharacterController>();
        if (controller.isGrounded) {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection = moveDirection * speed;

            if (Input.GetButtonDown("Jump")) {
                moveDirection.y = jumpForce;
            }
        }
        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);
    }

    void OnTriggerEnter(Collider collider) {
        if (collider.name == "Gold") {
            FindObjectOfType<GameManager>().MazeComplete();
        }
    }
}
