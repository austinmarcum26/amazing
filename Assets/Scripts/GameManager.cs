﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject completeMazeUi;

    public void MazeComplete() {
        completeMazeUi.SetActive(true);
    }


}
